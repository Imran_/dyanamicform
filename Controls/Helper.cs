﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
namespace Controls
{
    [Obfuscation(Exclude=false)]
    static class Helper
    {
        /// <summary>
        /// Extender of MemberInfo, Returns the datatype ex String or Int32
        /// </summary>
        /// <param name="t">MemberInfo</param>
        /// <returns>Type in string. for example Int,string</returns>
        public static string DataType(this MemberInfo t) 
        {
          
        var name=t.ToString().Split(' ');
        var data = name[0].Split('.');
        var c = data.Count();
        return data[c-1];        
        }
        /// <summary>
        /// Returns A valid Control for the datatype
        /// </summary>
        /// <param name="Info">Member Info</param>
        /// <returns></returns>
        public static LabeledControl ReturnControl(this MemberInfo Info,object Model,PropertyInfo Property)
        {
              var infos = System.Attribute.GetCustomAttributes(Info);
              ValidationTextBox control= new ValidationTextBox();
              System.Windows.Forms.Label Label = new System.Windows.Forms.Label();
              Label.Text = Info.Name;   
              control.Validate = false;
              control.Text=Property.GetValue(Model, null).ToString();
              control.Validated += new System.EventHandler(control.Vaildate_This);
              #region Attributes
              foreach (var att in infos)
              {
                  #region RegularExpression
                  if (att is RegularExpressionAttribute)
                  {
                      var a = (RegularExpressionAttribute)att;
                      control.VaidationExpression = a.Pattern;
                      control.ValidationMessage = a.ErrorMessage;
                      control.Validate = true;
                  }
                  #endregion
                  #region Display 
                  if (att is DisplayAttribute)
                  {
                      var a = (DisplayAttribute)att;
                      Label.Text = a.Name;
                  }
                  #endregion
                  #region Tye Of Text Box
                  if (att is DataTypeAttribute)
                  {
                      var a = (DataTypeAttribute)att;
                      //MessageBox.Show(a.DataType.ToString());
                      switch (a.DataType)
                      {
                          case System.ComponentModel.DataAnnotations.DataType.Currency:

                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Custom:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Date:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.DateTime:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Duration:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.EmailAddress:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Html:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.ImageUrl:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.MultilineText:
                              control.Multiline = true;
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Password:
                              control.PasswordChar = '*';
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.PhoneNumber:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Text:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Time:
                              break;
                          case System.ComponentModel.DataAnnotations.DataType.Url:
                              break;
                          default:
                              break;
                      }




                  }
                  #endregion
              }
              #endregion
              var p= new Dictionary<string, string>();
            p.Add(Info.Name,Info.ToString());

          return new LabeledControl { Control = control, Label = Label, ModelBilnter =p  };
        }
        public static LabeledControl ReturnControl(this MemberInfo Info)
        {
            var infos = System.Attribute.GetCustomAttributes(Info);
            ValidationTextBox control = new ValidationTextBox();
            System.Windows.Forms.Label Label = new System.Windows.Forms.Label();
            Label.Text = Info.Name;
            control.Validate = false;
            control.Validated += new System.EventHandler(control.Vaildate_This);
            #region Attributes
            foreach (var att in infos)
            {
                #region RegularExpression
                if (att is RegularExpressionAttribute)
                {
                    var a = (RegularExpressionAttribute)att;
                    control.VaidationExpression = a.Pattern;
                    control.ValidationMessage = a.ErrorMessage;
                    control.Validate = true;
                }
                #endregion
                #region Display
                if (att is DisplayAttribute)
                {
                    var a = (DisplayAttribute)att;
                    Label.Text = a.Name;
                }
                #endregion
                #region Tye Of Text Box
                if (att is DataTypeAttribute)
                {
                    var a = (DataTypeAttribute)att;
                    //MessageBox.Show(a.DataType.ToString());
                    switch (a.DataType)
                    {
                        case System.ComponentModel.DataAnnotations.DataType.Currency:

                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Custom:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Date:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.DateTime:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Duration:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.EmailAddress:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Html:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.ImageUrl:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.MultilineText:
                            control.Multiline = true;
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Password:
                            control.PasswordChar = '*';
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.PhoneNumber:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Text:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Time:
                            break;
                        case System.ComponentModel.DataAnnotations.DataType.Url:
                            break;
                        default:
                            break;
                    }




                }
                #endregion
            }
            #endregion
            var p = new Dictionary<string, string>();
            p.Add(Info.Name, Info.ToString());

            return new LabeledControl { Control = control, Label = Label, ModelBilnter = p };
        }
        public static object Value(this ValidationTextBox Textbox,string datatype) {
            switch (datatype.ToLower())
            {
                case "int32":
                    return Convert.ToInt32(Textbox.Text);
                case "string":
                    return Textbox.Text;
                default:
                    return null;
                    
            }
        
        }

    }
}
