﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.CSharp;
using System.Dynamic;
using System.ComponentModel.DataAnnotations;

namespace Controls
{
    
    public partial class DyanamicComponent<T> : Component where T:class
    {
        public T Model { get; set; }
        public List<ValidationTextBox> Controls;
        public List<Label> Labels;
        public int Count;
        public ToolTip ToolTipControl;
        private Dictionary<string, string> key;
        public DyanamicComponent(T _Model) {

            Model = _Model;
            AddControls();
            InitializeComponent();
            
        }
        public DyanamicComponent()
        {
            comp();
            
            InitializeComponent();
        }

        private void AddControls()
        {
            ToolTipControl = new ToolTip();
            Controls = new List<ValidationTextBox>();
            Labels = new List<Label>();
            TableLayout = new TableLayoutPanel();
            key = new Dictionary<string, string>();
            object ob = Model;
            var t = typeof(T);
            Count = 0;
            if (t.IsClass)
            {
                var members = t.GetMembers();

                foreach (var i in members)
                {

                    if (i.MemberType == MemberTypes.Property)
                    {
                        var p=i.ReturnControl(ob,t.GetProperty(i.Name));
                        Controls.Add(p.Control);
                        Labels.Add(p.Label);
                        key.Add(p.ModelBilnter.First().Key, p.ModelBilnter.First().Value);

                        Count++;
                     
                    }
                }
            }
        TableLayout.RowCount = Count;
        }
        private void comp()
        {
            ToolTipControl = new ToolTip();
            Controls = new List<ValidationTextBox>();
            Labels = new List<Label>();
            TableLayout = new TableLayoutPanel();
            Count = 0;
            object ob = Model;
            var t = typeof(T);
            if (t.IsClass)
            {
                var members = t.GetMembers();

                foreach (var i in members)
                {

                    if (i.MemberType == MemberTypes.Property)
                    {
                        var p = i.ReturnControl();
                        Controls.Add(p.Control);
                        Labels.Add(p.Label);
                        Count++;
                    }

                }

            }
            else
            {
                throw new Exception("Not a entity");
            }
            TableLayout.RowCount = Count;
            
        
        }
        public DyanamicComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        public T ReturnData()
        {
            object ob = Model;
            var t = typeof(T);
            int count = 0;
            if (t.IsClass)
            {
                var members = t.GetMembers();

                foreach (var i in members)
                {

                    if (i.MemberType == MemberTypes.Property)
                    {
                        var p = t.GetProperty(i.Name);
                        if (p.CanWrite) {
                            var data =i.DataType();

                            
                            p.SetValue(ob, Controls[count].Value(data), null);
                            count++;
                        
                        }

                    }
                }
            }

            return Model;
        }
    }
}
