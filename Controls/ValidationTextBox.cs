﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Drawing;

namespace Controls
{
    public partial class ValidationTextBox : TextBox
    {
        public string ValidationMessage { get; set; }
        public string VaidationExpression { get; set; }
        public List<string> Data{get;set;}
        public bool Validate { get; set; }
        public bool ValidationResult { get; set; }
        public ToolTip T;
        public BorderStyle back;
        public ValidationTextBox()
        {
            
            T = new ToolTip();
            back = this.BorderStyle;
            InitializeComponent();

        }
        
        public ValidationTextBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        public void Vaildate_This(object sender, EventArgs e) {
           
            T.AutomaticDelay = 0;
            T.BackColor = Color.Red;
            if (!Validate)
                return;
            Regex r = new Regex(VaidationExpression);
           ValidationResult= r.IsMatch(this.Text);
           if (!ValidationResult)
               this.BackColor = Color.Pink;
           else
               this.BackColor = Color.White;
           T.SetToolTip(this, ValidationMessage);
        }
    }
}
