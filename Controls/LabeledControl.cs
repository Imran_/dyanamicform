﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Controls
{
    public class LabeledControl
    {
        public ValidationTextBox Control { get; set; }
        public Label Label { get; set; }
        public Dictionary<string, string> ModelBilnter { get; set; }
    }
}
