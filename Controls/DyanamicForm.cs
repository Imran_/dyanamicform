﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Controls
{
    public partial class DyanamicForm<T> : UserControl where T : class
    {
        protected T _Model;
        public T Model {
            get { return _Model; }
            set { _Model = value; }
        }
        public DyanamicForm()
        {
            InitializeComponent();
        }
    }
}
