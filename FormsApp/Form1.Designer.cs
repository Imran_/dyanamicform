﻿namespace FormsApp
{
    using Entity;
    using Controls;
    using System.Windows.Forms;
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
                base.OnPaint(e);
            
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeComponent()
        {
            
            this.Tip = new ToolTip(); this.Tip.AutomaticDelay = 0;
            this.button1 = new Button();
            this.panel.RowCount = 2;
            this.panel.ColumnCount = 1;
            this.SuspendLayout();
            // 
            // userControl11
            // 
            var controls = this.userControl11.Controls;
            var labe = this.userControl11.Labels;
            int ip = 0;
            int count = this.userControl11.Count;
            var table = this.userControl11.TableLayout;
            table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            table.Dock = System.Windows.Forms.DockStyle.Fill;
            panel.Dock = System.Windows.Forms.DockStyle.Fill;
            table.Location = new System.Drawing.Point(0, 0);
            foreach (var i in controls)
            {

                i.Location = new System.Drawing.Point(1, 0 + ip * 20);
                i.Size = new System.Drawing.Size(240, 211);
                i.TabIndex = ip;
                labe[ip].Location = new System.Drawing.Point(12, 0 + ip * 20);
                //i.Click += new System.EventHandler(i.v);
                labe[ip].Size = new System.Drawing.Size(100, 20);
                table.Controls.Add(labe[ip],0,ip);
                table.Controls.Add(i,1,ip);
               
              
                ip++;
                this.Tip.SetToolTip(i, i.ValidationMessage);
            }
            this.panel.Controls.Add(table, 0, 0);


           
            //this.components.Add(this.userControl11.ToolTipControl );
            // 
            // Form1
            // 
            //this.button1.Location = new System.Drawing.Point(797, 227);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Get The values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
           // this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 262);
            this.panel.Controls.Add(this.button1,0,1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ClientSize = new System.Drawing.Size(766, 392);
            this.Controls.Add(panel);
            this.ResumeLayout(false);

        }

        #endregion
        private ToolTip Tip;
        public TableLayoutPanel panel = new TableLayoutPanel();
        private System.Windows.Forms.Button button1;
        private Controls.DyanamicComponent<Student> userControl11 = new Controls.DyanamicComponent<Student>(new Student { Name="1234567890",  Address="address", Id=2});
    }
}

