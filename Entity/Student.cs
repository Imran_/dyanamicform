﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace Entity
{
    public class Student
    {
        [Browsable(true),Category("Identity")]
        public int Id { get; set; }
        [Display( Name="Name of The person")]
        [RegularExpression(@"\d[10]",ErrorMessage="Galat Number")]
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
       
    }
}
