// <auto-generated />
namespace Entity.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class student : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(student));
        
        string IMigrationMetadata.Id
        {
            get { return "201301051837554_student"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
